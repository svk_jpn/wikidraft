他の格闘ゲーム同様に通常技をキャンセルして必殺技につなぐことが出来る。  
一般的な格闘ゲームとのキャンセルルールの違いについて説明する。  

**目次**  

[[_TOC_]]

# 1. キャンセルルール
サイバーボッツではキャンセル可能技からキャンセル対応技にキャンセルが可能である。

## 1.1.キャンセル可能技
キャンセル可能技(動作を途中キャンセルしてキャンセル対応技に移行できる)は以下である。

- 通常技  
立ち、しゃがみ、ジャンプ、ダッシュから最大1つまでの方向入力と攻撃ボタン(L, H, W)で出る技。
- コンビネーション
- **諸行無常**(ゲイツ)

## 1.2.キャンセル対応技
キャンセル対応技(キャンセル可能技をキャンセルできる)は以下である。

- コマンド投げを除く必殺技
- サイバーEX
- パワーチャージ
- ギガクラッシュ

## 1.3.キャンセル可能時間
技発生後一定フレーム内であれば、キャンセル可能。  

ヒット、ガード、空振り関係なく技が出てから技毎の可能時間内であればキャンセル可能。  
かなり珍しい仕様で、あまりにも適当である。  
このルールに伴うプレイ中の注意点を以下に挙げる

### 1.3.1.多段技キャンセル必殺技に注意が必要
多段技をキャンセルして発生の遅い技につなぐ場合に、技がヒットしたタイミングで次の技入力を完成させないとつながらないといった現象が起きる。  
例としては、2hit技の2hit目が出る直前でキャンセルすると2hit目が出なくなり、隙間が大きくなることでつながらなくなる、などが挙げられる。  
他のゲームでは、ヒット、ガード時しかキャンせるがかからないケースが多いため問題になりづらい。  

### 1.3.2.空振りキャンセルによる隙消し
隙の大きい技を隙の小さい技でキャンセルすることで隙消しが可能。  
他のゲームの空振りキャンセル可能フレームは多くても2,3フレーム程度で、攻撃判定発生後に出来るものはほぼ無い。  
サイバーボッツでは技によるが、10フレーム以上キャンセル可能時間があるものある。  
牽制技を判定が出た直後にパワーチャージでキャンセルして即止めで後隙を軽減する、は様式美である。  

# 2.コンビネーション
特定の順番、タイミングで押すことで、通常技から通常技の派生技を出すことができるシステム。  
ルートは基本共通であるが一部の機体はルートが異なる。  
コンビネーションを所有していない機体も存在する。  

## 2.1.コンビネーションルート
以下、角の丸い四角は始動技。

**通常ルート**
```mermaid
graph LR

id1(L) --> id2[L] --> id3[H]
id1-->id3
```

**ソードマンダッシュ中ルート**
```mermaid
graph LR

id1(bL) --> id2[L] --> id3[H]
id1-->id3
```

**ブロディア**
```mermaid
graph LR
  id1(L) --> id2[L] --> id4[H]
  id1 --> id3(2L) --> id4
  id1 --> id4[H]
  id1 --> id5[2H]
  id2 --> id5
  id3 --> id5
```

**ジャッカル**
```mermaid
graph LR

id1(L) --> id2[L] --> id3[H]
```

**ゲイツ**  
点線の矢印部は<font color="red">連続ヒットしない</font>。  
何につかえと…。  

```mermaid
graph LR

id1(L) -.-> id2[L] --> id3[H]
```

**ワーロック**
```mermaid
graph LR
  id1(L) --> id1
```

コンビネーションの始動技はメインアーム(取れる方の腕)攻撃なので、腕無し時はコンビネーションは使用不可となる。  
唯一例外として、ブロディアの2Lに関してはサブアーム攻撃なので、ブロディアは腕が取れてもコンビネーションが使え、さらにバグっぽく2L>Hのルートでは腕が無い状態で殴ります(ちゃんと判定もあり)。

## 2.2.キャンセル可能時間
コンビネーションのキャンセル可能時間はコンビネーションの始動技からの時間で判定される。  
全段当てからのキャンセル必殺技で〆は、コンビネーションの派生及び、キャンセルを最速で行わねばならない。  
ほとんどのキャラはコンビネーションの最終段まではキャンセル可能時間は残らない。  
実用の観点からいうと、L>H>623H のように真ん中を省いて2段からキャンセルすることを強く推奨。  

# 3.ガードキャンセル
地上ガードのヒットストップ終了直後にガード硬直をキャンセルしてバックブーストを出すことができる。  
バックブーストには短い無敵(6F)が付与される。  
多段技をガード時に場合に後ろを入れながらBを連打する使い方が多い。  
削られる回数をへらしたり、場合によっては後退により技から抜け出すこともできる。  
ガードキャンセルバックブースト中も、通常のバックブースト同様の行動が可能なため、発生の早い技などを出すことで割り込むこともできる。

# 4.その他
その他のキャンセル
## 4.1.ジャンプによる地上ダッシュキャンセル
特に不思議でもないが、ダッシュ中にジャンプできる。  
ジャンプした時点でジャンプモーションに入るため、ブースト稼働中で無くなるのがポイント。  
ただのジャンプモーションになるため、キャンセルして投げ技を出すことができる。  

## 4.2.着地硬直キャンセル
ジャンプの着地には硬直があり、再ジャンプはすぐには出来ない。  
この硬直は各種攻撃やガード、地上ブーストでキャンセルが可能である。  
素早く再ジャンプしたい場合は、地上ブーストしてダッシュキャンセルジャンプを行う(Touch & Go)。  