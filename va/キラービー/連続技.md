キラービーの連続技。  

[キラービー攻略トップへ戻る](/va/キラービー/攻略トップ)

#### 1. L > L > H
基本コンボでありながら一番火力が出る。  
チェーンの入力がシビアなので要練習。  

#### 1. (6b>bc>) 2L > 2L > 236L
2Lの持続当てからは2Lが繋がる。  
端端状況で前ブーストから慣性を残して2Lを出すと状況を作りやすいが、あまり使う機会はない。