タランテラの攻略記事のトップページ。

[[_TOC_]]

# 1.機体説明
比較的素直で使いやすい機体。  
足の速さと、打撃択の強さから素直な投打2択を迫っていける。  
判定の強い技も多く初心者にも扱いやすい機体。

## 1.1.サマリー
1(低評価) -- 5(高評価) でのざっくりスペック。  

|項目|評価|備考|
|:--|:--|:--|
|防御力|2|ステータス上の防御力が低いのがネック。|
|触りやすさ|4|ウェポンのミサイルで露払いをしつつ接近が仕掛けられる。|
|逃げやすさ|3|ジャンプ回数4と足の速さで比較的逃げやすい。|
|対空中|4|ダメージが高いとは言えないが一応実用的な対空中削りを持つ。<br>サイバーEXの対空投げも加点。|
|牽制能力|4|主力のbLやパワードレッカーの判定が全体的に強い。|
|切り返し能力|2|切り返しに適した技は無いのが不安。|
|削り能力|3|削り総量は小さいが、地上空中ともに狙えるのでこの程度。|
|崩し能力|4|bLのプレッシャーと足の速さで地上択は強い。|
|火力|4|確認が楽で威力の高いコンボを持ち安定して高い。|

## 1.2.長所
隙が小さく、判定が強く、コンボから簡単に大ダメージが奪えるbL。  
滑りの長い地上ダッシュから狙えるダッシュ投げと速い下段の2Lで地上の攻めは万全。  
攻めのきっかけをサポートするウェポンも強く。  
中距離では判定が強く、掴めば大ダメージのパワードレッカーもある。

## 1.3.短所
食らい判定が大きめで、防御力自体が低い。  
主力のダメージが取れる状況が近距離であるため、きっちりリスク管理をしないと、事故で大ダメージをもらいがち。  
隙の小さめなbLも最近ではガードキャンセルで割られることも。  

# 2.技解説

## 2.1.コマンド一覧
|名称|コマンド|説明|
|:--|:--|:--|
|通常投げ|(近距離)4 or 6 or 7 or 9とA|L, Hで投げの内容は異なる。|
|ダウン追い打ち投げ|(相手ダウン, 近距離)<br>4 or 6 or 7 or 9とH||
|アームリッパー|(近距離)632A|アームダメージ主眼の投げ。|
|ストンピング|空中で1 or 2 or 3とH|主にダウン追い打ち用。|
|パワードレッカー(地上)|236A|腕を伸ばす打撃掴み。|
|パワードレッカー(空中)|(空中で)236A or 632A|空中で腕を伸ばす打撃掴み。|
|叩きつけ|(地上, 各種パワードレッカー掴み成立直後)<br>A|飛び上がって地面に叩きつける追加攻撃。|
|サウザンドミサイル|(地上, 各種パワードレッカー掴み成立直後)<br>214A|相手を放り投げミサイルを多量に発射する。|
|ライジングトルネード|623A or 421A|飛び上がりながら回転させた脚で攻撃。|
|タランテラスタンプ|2溜め8A|相手をサーチする中段攻撃。|
|フォーリングデストロイ|(要ゲージ)23623A|飛び上がっての対空投げと着地後の地上投げ。|
|ギガクラッシュ|(要ゲージ)LH|システム切り返し技。|

コンビネーションルート  
```mermaid
graph LR

id1(L) --> id2[L] --> id3[H]
id1-->id3
```
**(角の丸い四角は始動技)**

一般的なコンビネーション。  
Hの部分はダウンが奪える。2段目のLがヒット時の有利が大きい(+10F程度)。  
初段の発生が9Fと遅いのがネックであるが投げとの自動二択で使用していく。  

## 2.2.通常技解説

**L**  
短い打撃(発生9F)。爪部にくらい判定が無い。  
コンビネーション始動技としてはかなり遅い方で切り返しに不安がある。  

**2L**  
発生の速い短い下段(発生6F)。  
単発で心もとないが、下段でタランテラの切り返しの要。  

**bL**  
足を回転させての多段突進(発生8F)。  
ガードされても隙が少なく、ヒット時は目押しからライジングトルネードがつながる。  
接近、コンボの起点で、1Hit目をチャージキャンセルすれば有利も取れる。  
2段目3段目の間はガードキャンセルで割り込み可能なので狙われている場合はチャージキャンセルも混ぜたい。

**jL**  
短い飛込み(発生5F)。  
発生の早さがウリ。  

**H**  
爪を振り上げる2Hit技(発生7F)。  
Lより発生が早いので投げ暴れをこちらでやるのもあり。  
端密着ならば初段チャージキャンセルで延々入る(高難易度)。  

**2H**  
電撃による長い下段(発生 16F)。  
20F辺りの電撃部分に食らい判定が無い。  
チャージキャンセルは攻撃発生直後(まだ伸びきっていない)までは可能。  
バックブースト狩りに使えなくもないがトドメぐらいか。  

**bH**  
足をドリルに変形させての多段突進(発生 11F)。最大4Hit。  
2Hit目までキャンセル可能。ブーストの方向によらず前進する。  

**jH**  
足を斜め下方向に射出する(発生16F)。  
伸びきったところのリーチと判定が強い。  
飛込みの主力。

**j2H**  
ストンピング(発生 9F, 最大3Hit)。  
多段ジャンプ型は攻めでは当てづらい。  

**W**  
ウェポンゲージ100%消費(発生 12F)。  
前方に小型ミサイルを2発射出。着弾で爆風が起きる。  
ミサイルが見づらく、爆風も攻撃判定があるので出てしまえば強い。  
受付は短いが射出後チャージキャンセルも可能。  
爆風ののけぞり時間が長いので、固めやコンボに行ける。  

**2W**  
ウェポンゲージ100%消費(発生 12F)。  
高い放物線状にミサイルを射出。  
着弾まで時間があるので起き攻めなどに向く。  

**jW**  
ウェポンゲージ100%消費(発生 12F)。  
横向きにミサイルを射出。  
ミサイルポッドを画面外(上)に隠すと射出タイミングがわかりづらくなる。  
遠間からの牽制に。  

**j2W**  
ウェポンゲージ100%消費(発生 12F)。  
足元付近にミサイルを射出。  
ミサイルポッドを画面外(上)に隠すと射出タイミングがわかりづらくなる。  
こちらは近距離での運用となる。  

**通常投げ**  
Lは掴んでのヘッドバッド威力が低い。  
Hは放り投げでL投げより威力が高い。  
LよりHの発生が早いことからタランテラはH投げの方が使いやすい。  

**ダウン投げ**  
タランテラのダウン投げはHのみ。  
メイン追い討ち技である。

**アームリッパー**  
アームダメージ重視の投げ。  
タランテラは通常投げのダメージが低めなため、アームリッパーも相対的に悪くない。  

## 2.3.必殺技解説

**パワードレッカー(地上)**  
**パワードレッカー(空中)**  
腕を伸ばして掴む打撃掴み(発生 6F)。  
打撃部もダウン属性なので、ガードさせなければリスクは無い。  
地上版はLが真横、Hが斜め上45°、LHが真上に腕を伸ばす。  
空中版は236で出した場合は地上と同じ方向、632で出した場合はその上下が反転した方向に腕を伸ばす。  
掴んだ場合は地面に数回たたきつけるが、威力は低く、追加入力でダメージを上げる必要がある。  
爪に食らい判定が無く、先端で牽制に使うのもあり。  

地上版はダッシュ慣性が載る。また、コマンド成立直後にBを押すと腕を伸ばしたまま走る。  
空中版の水平および水平より上方向に腕を伸ばしたあとは着地までなぜか打撃無敵になる。  

**叩きつけ**  
**サウザンドミサイル**  
パワードレッカーからの追加技。  
入力タイミングは掴んだ直後かつ地面に居るタイミング。  
地上は素早く入力する必要があるが、空中は掴んで着地したタイミングでコマンドが成立するようにする。  
サウザンドミサイル>叩きつけ>入力無し の順でダメージと状況が良いため可能な限りサウザンドミサイルを推奨。  
着地より若干早めに214をいれてA連打が出しやすいだろう。  

**ライジングトルネード**  
bLのような攻撃を飛び上がりながら出す。(発生 623L:5F, 623H:5F, 421L:11F, 421H:15F)  
623は前方上方に飛び上がり、421は後方に飛び上がってから前方下方か前方水平方向に突進する。  
前方版を連続技に使うのが主で、bLからノーキャンセルで簡単につながり大ダメージが取れる。  

**タランテラスタンプ**  
相手をサーチして飛び上がる中段踏みつけ。  
ダウン追い打ち判定あり。基本的に2Wをキャンセルして出しCPUをハメる技。  

**フォーリングデストロイ**  
サイバーEX(要パワーゲージ, 発生 暗転後8F)。  
飛び上がって空中の相手を掴む、空中投げ技。  
着地時には地上投げに移行する。  
ダッシュ慣性が乗り、23623b,Aで出すとちょっと長くなる。

**ギガクラッシュ**  
システム切り返し技(要パワーゲージ)(発生15F)。  
発生は遅いが、無敵が打撃発生直後まで続く。  
切り返しの要。

# 3.攻略詳細へのリンク
- [テクニック](/va/タランテラ/テクニック)
- [連続技](/va/タランテラ/連続技)
- [初心者向けコンボ/連携](/va/タランテラ/初心者向け)
