Markdownの練習用に使ってください。あとは、書き方のメモとか(GitLabの説明が英語なので)  
編集の際にはDeveloper権限が必要なので[管理人](https://twitter.com/svk_jpn)まで連絡をください。  
事前にGitLabのアカウントを作成してもらえるとスムーズです(そうでないとメールアドレスが必要)。  

==== ↓ここから先は自由に編集して良いです。 ====

[[_TOC_]]

# 1. 数式の書き方。
(1) インライン
たとえば$`a^2 = b^2 + c^2`$とか

(2) 数式行
```math
f(n)=\sum_{i=0}^n \frac{1}{i}^3
```

mermaidでブロディアのコンビネーションルートを記述。
```mermaid
graph LR
  id1(L) --> id2[L] --> id4[H]
  id1 --> id3(2L) --> id4
  id1 --> id4[H]
  id1 --> id5[2H]
  id2 --> id5
  id3 --> id5
```

# 2. ページ内リンク

ページ内リンクの書き方  
[サンプル リンク](#21-サンプル-リンク)

## 2.1 サンプル リンク
スペースは-に置換する。 
.(ピリオド)は消去する。

# 3. 画像にリンクをつけてみる

↓shoryukenに飛びます

[![Botslogo](uploads/d3b189b0e643cdf41f097826724fdfd2/Botslogo.png)](http://wiki.shoryuken.com/Cyberbots:_Fullmetal_Madness)

wikiリポジトリ内の動画
![sample movie](/va/ソードマン/movies/sword_combo6.mp4)

# 4. お砂場
- GitLab wikiの書き方基礎<br>
    [GitLabのMarkdownチートシート](https://docs.gitlab.com/ee/user/markdown.html)<br>
    改行がむずいので\<br>を使うのが賢いかも。<br>

- エディタについて<br>
	軽く試してから気付いたけど、web上のエディタが見にくい。web上で編集$`\rightarrow`$更新$`\rightarrow`$確認$`\rightarrow`$...の流れは、履歴が残る点からしてもあまり賢くない。<br>
	$`\Rightarrow`$適当なMarkdownエディタをぶっこんでオフライン編集が妥当<br>
	- [Typora](https://www.typora.io/)<br>
		直感的に操作できますよってやつ。ライブプレビュー式（成果物のプレビューをいじってソースコードを出す）。インストール簡単。個人的には使いにくかった。
	- [Atom](https://atom.io/)<br>
		GitHub社の提供するエディタ。色々できるらしい。インストールからスタートまでが少し手間。でもいいかんじ。[ここ](https://qiita.com/kouichi-c-nakamura/items/5b04fb1a127aac8ba3b0)がわかりやすかった。
	- [Visual Studio Code(VSCode)](https://code.visualstudio.com/)  
		Microsoft社の提供するエディタ。GitHubはMicrosoft傘下である。プラグインが豊富でgit連携も良くできてる。  
		Gitの触りだけなら[このへん](https://qiita.com/mnao305/items/b3c5f5943066a0bb8e2e)みましょ。
<br>

- 上手いインデントの取り方を知りたい...<br>
  たぶんエディタ使うのが一番楽。

- mathってどこまでできるん？<br>
    GitLabではKaTeXが用いられている。<br>
    $`\rightarrow`$[KaTeXのサイト](https://katex.org/)で結果を見るのが一番早いが...表示結果がちょっと違うっぽい（若干潰れる）。<br>

    ex)　\oint_{S}d\mathbf{S}\bullet\mathbf{A}=\int_{V}dV\nabla\bullet\mathbf{A}<br>
	```math
    \oint_{S}d\mathbf{S}\bullet\mathbf{A}=\int_{V}dV\nabla\bullet\mathbf{A}
	```
  
# 5. wiki編集用

- mizuumi wikiについて<br>
    Hildrさんの作っている[mizuumi wiki内の記事](http://wiki.mizuumi.net/w/Cyberbots)<br>
    記事ごとにフォーマットにブレはありますが、wikiの一例として。[ヴァイスのページ](http://wiki.mizuumi.net/w/Cyberbots/Vise)がおしゃれですけど、今なら作れちゃうのかなって気もします。<br>

- 表記について<br>
    立体の方がそれっぽい気が。<br>
    
    |TeX記述|結果|
    |:-:|:-|
    |A_1|$`A_1`$|
    |\mathrm{A_1}|$`\mathrm{A_1}`$|

- フレーム表的な<br>
  markdownのtable記述では難しいものはhtmlで書いてしまうほうがたぶん楽。

  <table>
  <tr>
    <td rowspan="3" align="center" valign="top">j.L<br>[image]</td>
    <td align="center">guard</td>
    <td align="center">st.</td>
    <td align="center">act.</td>
    <td align="center">rcv.</td>
    <td align="center">adv.</td>
    <td align="center">inv.</td>
  </tr>
  <tr>
    <td align="center">m, h, h</td>
    <td align="center">3</td>
    <td align="center">16</td>
    <td align="center">land</td>
    <td align="center">-(h), -(g)</td>
    <td align="center">-</td>
  </tr>
  <tr>
    <td colspan=6>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </td>
  </tr>
  </table>
